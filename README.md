# YVisualizer

This project is a web application that displays and interacts with data from a social network simulation. It includes a Flask backend providing data via APIs and a JavaScript frontend for visualization and interactivity.

## Features
  
### Backend

 - API Endpoints:
	 - /post: Fetches all posts.
	- /user_mgmt: Fetches all users.
	- /reactions: Fetches all reactions (likes/dislikes).
	- /rounds: Fetches round-based timestamps.
	- /interests: Fetches users' interests.
- Database: Uses SQLite (local_test.db) to store the data.

### Frontend

- Dynamic Data Fetching:
	- Retrieves and displays posts, users, and reactions.

- Interactive Features:
	- Search posts by username.
	- Sort posts by likes, dislikes, replies, or timestamps.
	- View replies to posts dynamically.

- Statistics:
	- Total number of users and posts.
	- Average reactions per post.
	- Most active user by post count.
	- List of unique topics/interests.

### Technologies Used

- Backend: Python, Flask, SQLite.
- Frontend: HTML, CSS, JavaScript (jQuery included).

### Setup and Usage
#### Prerequisites
- Python 3.x
- SQLite3
- Node.js or any static server (optional for frontend testing)

#### Steps

1. Clone the Repository:
`git clone <repository_url>`
`cd <repository_directory>`

2. Set Up Backend:
- Install dependencies:
`pip install flask flask-cors`
- Create local_test.db with appropriate tables and data.
- Start the API server:
`python app.py`

3. Set Up Frontend:
	- Open index.html in a browser.
	- Ensure the API URL in api variable matches your backend.