let api = "http://127.0.0.1:5000";
let globalUsers = [];
let globalPosts = [];
let globalReactions = [];
let globalRounds = [];
let globalInterests = [];

// Function to retrieve data from a specific URL
async function fetchData(url) {
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`HTTP error ${response.status}`);
        }
        return await response.json();
    } catch (error) {
        console.error(`Error fetching data from ${url}:`, error);
        return [];
    }
}

// Main function to initialize the display of posts and users
async function initialize() {
    try {
        [globalUsers, globalPosts, globalReactions, globalRounds, globalInterests] = await Promise.all([
            fetchData(api + '/user_mgmt'),
            fetchData(api + '/post'),
            fetchData(api + '/reactions'),
            fetchData(api + '/rounds'),
            fetchData(api + '/interests')
        ]);

        console.log("Users:", globalUsers);
        console.log("Posts:", globalPosts);
        console.log("Reactions:", globalReactions);
        console.log("Rounds:", globalRounds);
        console.log("Interests:", globalInterests);

        // STATISTICS
        const userCount = globalUsers.length;
        const postCount = globalPosts.length;
        const averageReactions = postCount > 0 ? (globalReactions.length / postCount).toFixed(2) : 0;

        // Most Active User
        const userPostCount = globalPosts.reduce((acc, post) => {
            acc[post.user_id] = (acc[post.user_id] || 0) + 1;
            return acc;
        }, {});
        const mostActiveUserId = Object.keys(userPostCount).reduce((a, b) => userPostCount[a] > userPostCount[b] ? a : b);
        const mostActiveUser = globalUsers.find(user => user.id === parseInt(mostActiveUserId));

        document.getElementById("user-count").textContent = `Number of Users: ${userCount}`;
        document.getElementById("post-count").textContent = `Number of Posts: ${postCount}`;
        document.getElementById("average-reactions").textContent = `Average Reactions per Post: ${averageReactions}`;
        document.getElementById("most-active-user").textContent = `Most Active User: ${mostActiveUser.username}`;

        // TOPICS
        const uniqueTopics = [...new Set(globalInterests.map(interest => interest.interest))];
        const topics = uniqueTopics.join(', ');
        document.getElementById("topics").textContent = topics || "No topics available";

        const userId = getUserIdFromUrl();
        if (userId) {
            globalUsers = globalUsers.filter(user => user.id === parseInt(userId));
            document.getElementById("user-name").textContent = `${globalUsers[0].username}`;
        } else {
            console.log("User ID not found in the URL.");
        }

        displayPosts();
        setupSearch();
        setupSort();
    } catch (error) {
        console.error('Error initializing the page:', error);
    }
}

document.addEventListener('DOMContentLoaded', (event) => {
    initialize();
});

function setupSearch() {
    const searchBar = document.getElementById('search-bar');
    if (searchBar) {
        searchBar.addEventListener('input', function(event) {
            const query = event.target.value;
            query.length > 0 ? searchPostsByUser(query) : displayPosts();
        });
    }
}

// Function to configure sorting
function setupSort() {
    const sortSelect = document.getElementById('sort-select');
    sortSelect.addEventListener('change', function(event) {
        handleSort(event.target.value);
    });
}

// Function to get user ID from URL
function getUserIdFromUrl() {
    return new URLSearchParams(window.location.search).get('user_id');
}

// Function to calculate the time elapsed since a round
function getTotalHoursAgo(rounds, currentRound) {
    let finalRound = rounds[rounds.length - 1];
    return (24 * finalRound.day + finalRound.hour) - (24 * currentRound.day + currentRound.hour);
}

// Function to generate HTML for a post
function generatePostHtml(post) {
    let user = globalUsers.find(user => user.id === post.user_id);
    let round = globalRounds.find(r => r.id === post.round);
    if (!user || !round) return '';

    return `
        <div class="post-card" id="post-${post.id}">
            ${generatePostHeaderHtml(user, post, round)}
            ${generatePostContentHtml(post)}
            ${generatePostFooterHtml(post)}
            <div class="replies-container" id="replies-${post.id}" style="display: none;"></div>
        </div>
    `;
}

function generatePostHeaderHtml(user, post, round) {
    let totalHoursAgo = getTotalHoursAgo(globalRounds, round);
    let daysAgo = Math.floor(totalHoursAgo / 24);
    let hoursAgo = totalHoursAgo % 24;

    return `
        <div class="top">
            <div class="left">
                <h3 class="username">
                    <a href="user.html?user_id=${user.id}" class="username-link">@${user.username}</a>
                </h3>
                <small class="time">${daysAgo} day(s), ${hoursAgo} hour(s) ago</small>
            </div>
            <div class="right">
                <small class="post-id">${post.id}</small>
            </div>
        </div>
    `;
}

function generatePostContentHtml(post) {
    return `
        <div class="mid">
            <p class="content">${post.tweet}</p>
        </div>
    `;
}

function generatePostFooterHtml(post) {
    let postReactions = globalReactions.filter(reaction => reaction.post_id === post.id);
    let likes = postReactions.filter(reaction => reaction.type === 'like').length;
    let dislikes = postReactions.filter(reaction => reaction.type === 'dislike').length;

    return `
        <div class="bottom">
            <div class="likes-dislikes">
                <small class="likes">
                    <a href="">
                        <i class="fa-solid fa-thumbs-up"></i>
                        ${likes}
                    </a>
                </small>
                <small class="dislikes">
                    <a href="">
                        <i class="fa-solid fa-thumbs-down"></i> 
                        ${dislikes}
                    </a>
                </small>
            </div>
            <small class="replies">
                <a href="#" class="replies-link" data-post-id="${post.id}">
                    <i class="fa-regular fa-comment"></i>
                    ${countReplies(globalPosts)[post.id] || 0}
                </a>
            </small>
        </div>
    `;
}

// Event to display replies for a post
$(document).on('click', '.replies-link', async function(event) {
    event.preventDefault();
    const postId = $(this).data('post-id');
    const repliesContainer = $(`#replies-${postId}`);

    if (repliesContainer.is(':visible')) {
        repliesContainer.hide();
    } else {
        try {
            const repliesHtml = await getRepliesForPost(postId);
            repliesContainer.html(repliesHtml).show();
        } catch (error) {
            console.error(error);
        }
    }
});

// Function to get replies for a given post
async function getRepliesForPost(postId) {
    const replies = globalPosts.filter(post => post.comment_to === postId);
    return replies.map(reply => generatePostHtml(reply)).join('');
}

// Function to display posts
function displayPosts(posts = globalPosts) {
    let postsContainer = $('#posts-container');
    postsContainer.empty();
    posts.forEach(post => {
        if (post.comment_to === -1) {
            let postHtml = generatePostHtml(post);
            if (postHtml) postsContainer.append(postHtml);
        }
    });
}

// Function to count replies
function countReplies(posts) {
    return posts.reduce((acc, post) => {
        if (post.comment_to >= 0) {
            acc[post.comment_to] = (acc[post.comment_to] || 0) + 1;
        }
        return acc;
    }, {});
}

// Function to search posts by username
function searchPostsByUser(query) {
    const cleanedQuery = query.trim().toLowerCase();
    const regex = new RegExp(cleanedQuery, 'i');

    let matchingUsers = globalUsers.filter(user => regex.test(user.username.trim()));
    if (matchingUsers.length > 0) {
        let filteredPosts = globalPosts.filter(post => 
            matchingUsers.some(user => user.id === post.user_id)
        );
        displayPosts(filteredPosts);
    } else {
        $('#posts-container').html(`<p>No users found with the name '${query}'</p>`);
    }
}

// Function to sort posts
function sortPosts(sortOption) {
    let sortedPosts = [...globalPosts];
    const repliesCount = countReplies(globalPosts);

    switch (sortOption) {
        case 'old-first':
            return sortedPosts.sort((a, b) => a.round - b.round);
        case 'new-first':
            return sortedPosts.sort((a, b) => b.round - a.round);
        case 'likes':
            return sortedPosts.sort((a, b) => {
                let aLikes = globalReactions.filter(reaction => reaction.post_id === a.id && reaction.type === 'like').length;
                let bLikes = globalReactions.filter(reaction => reaction.post_id === b.id && reaction.type === 'like').length;
                return bLikes - aLikes;
            });
        case 'dislikes':
            return sortedPosts.sort((a, b) => {
                let aDislikes = globalReactions.filter(reaction => reaction.post_id === a.id && reaction.type === 'dislike').length;
                let bDislikes = globalReactions.filter(reaction => reaction.post_id === b.id && reaction.type === 'dislike').length;
                return bDislikes - aDislikes;
            });
        case 'max-replies':
            return sortedPosts.sort((a, b) => {
                let aReplies = repliesCount[a.id] || 0;
                let bReplies = repliesCount[b.id] || 0;
                return bReplies - aReplies; // Descending order
            });
        case 'min-replies':
            return sortedPosts.sort((a, b) => {
                let aReplies = repliesCount[a.id] || 0;
                let bReplies = repliesCount[b.id] || 0;
                return aReplies - bReplies; // Ascending order
            });
        default:
            return sortedPosts;
    }
}

// Function to display posts after sorting
function handleSort(sortOption) {
    let sortedPosts = sortPosts(sortOption);
    displayPosts(sortedPosts); // Display sorted posts
}