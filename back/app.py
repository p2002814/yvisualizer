from flask import Flask, jsonify
from flask_cors import CORS
import sqlite3

app = Flask(__name__)
CORS(app)

def get_db_connection():
    conn = sqlite3.connect('local_test.db')
    conn.row_factory = sqlite3.Row
    return conn

@app.route('/post')
def posts():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM post').fetchall()
    conn.close()
    return jsonify([dict(row) for row in posts])

@app.route('/user_mgmt')
def users():
    conn = get_db_connection()
    users = conn.execute('SELECT * FROM user_mgmt').fetchall()
    conn.close()
    return jsonify([dict(row) for row in users])

@app.route('/reactions')
def reactions():
    conn = get_db_connection()
    reactions = conn.execute('SELECT * FROM reactions').fetchall()
    conn.close()
    return jsonify([dict(row) for row in reactions])

@app.route('/rounds')
def rounds():
    conn = get_db_connection()
    rounds = conn.execute('SELECT * FROM rounds').fetchall()
    conn.close()
    return jsonify([dict(row) for row in rounds])

@app.route('/interests')
def interests():
    conn = get_db_connection()
    interests = conn.execute('SELECT * FROM interests').fetchall()
    conn.close()
    return jsonify([dict(row) for row in interests])

if __name__ == '__main__':
    app.run(debug=True)
